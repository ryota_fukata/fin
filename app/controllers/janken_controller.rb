class JankenController < ApplicationController
  def index
    @users = User.all
  end

  def point

    jankens = ["グー", "チョキ","パー"]
    program_hand = rand(3)
    player_hand = params[:player_hand].to_i
    @a = "あなたの手:#{jankens[player_hand]}, 相手の手:#{jankens[program_hand]}"
      if player_hand == program_hand
        var = true
      elsif(player_hand == 0 && program_hand == 1)||(player_hand == 1 && program_hand == 2)||(player_hand == 2 && program_hand == 0)
        @result = "あなたの勝ちです"
        var = false
      else
        @result = "あなたの負けです"
        var = false
      end

      if var == true
        flash.now[:aiko] = "あいこです。もう一回選択してください"
        render janken_janken_path
      else
        render janken_sen_path
      end
  end

  def sen
  end

  def janken
  end
end
