Rails.application.routes.draw do
  get 'janken/index'
  get 'janken/point'
  get 'janken/sen'
  get 'janken/janken'
  devise_for :users
  resources :top, :only => [:index]
  resources :posts, :only => [:show, :index, :new, :create, :edit, :update, :destroy]
  resources :users, :only => [:show, :index, :edit, :update]
  resources :messages, :only => [:create]
  resources :rooms, :only => [:create, :show, :index, :edit, :update, :destroy]
  root "top#index"
end
