require 'test_helper'

class JankenControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get janken_index_url
    assert_response :success
  end

  test "should get point" do
    get janken_point_url
    assert_response :success
  end

  test "should get sen" do
    get janken_sen_url
    assert_response :success
  end

  test "should get janken" do
    get janken_janken_url
    assert_response :success
  end

end
